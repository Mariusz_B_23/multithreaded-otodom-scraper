import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Otodom {

    private static final Logger LOGGER = Logger.getLogger(Otodom.class.getName());
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";
    private static final String REGEX = "https://www\\.otodom\\.pl/pl/oferta/[^\"]+";

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        String urlString = "https://www.otodom.pl/sprzedaz/mieszkanie/czestochowa/";

        try {
            Set<String> links = fetchLinks(urlString);
            links.forEach(link -> executorService.submit(() -> downloadPage(link)));
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "An error occurred while fetching links", e);
        } finally {
            executorService.shutdown();
        }
    }

    private static Set<String> fetchLinks(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("User-Agent", USER_AGENT);
        LOGGER.log(Level.INFO, "Response Code : {0}", conn.getResponseCode());

        Set<String> links = new TreeSet<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = reader.readLine()) != null) {
                response.append(inputLine).append(System.lineSeparator());
            }
            Pattern pattern = Pattern.compile(REGEX);
            Matcher matcher = pattern.matcher(response);
            while (matcher.find()) {
                links.add(matcher.group());
            }
        }
        return links;
    }

    private static void downloadPage(String link) {
        try {
            URL url = new URL(link);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("User-Agent", USER_AGENT);
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                 BufferedWriter writer = new BufferedWriter(new FileWriter(getFilenameFromLink(link)))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    writer.write(line);
                    writer.newLine();
                }
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Failed to download page from: {0}", link);
            LOGGER.log(Level.SEVERE, "Exception: ", e);
        }
    }

    private static String getFilenameFromLink(String link) {
        return link.substring(link.lastIndexOf('/') + 1) + ".html";
    }

}